.. _kuća:

=====================================
Kuća / Дом
=====================================

.. csv-table:: 
   :widths: 20, 40
   :delim: ;
   :header: "Srpski", "Ruski"

   ``kuća``; ``дом [dom]``
   ``soliter``; ``многоэтажка [mnogo-etažka]``
   ``sprat``; ``этаж [etaž]``
   ``zgrada``;``здание [zdanjije]``
   ``lift``;``лифт [ljift]``
   ``dvorište``;``двор [dvor]``
   ``krov``;``крыша [kr~iša]``
   ``prozor``;``окно [okno]``
   ``vrata``;``дверь [dvjer']``
   ``zid``;``стена [stjena]``
   ``stepenice``;``ступеньки [stupjen'ki]``
   ``stepenište``;``лестница [ljestnjica]``
   ``ulaz``;``подъезд [pod"jezd]``
   ``soba``;``комната [komnata]``
   ``hodnik``;``коридор [korjidor]``
   ``plafon``;``потолок [potolok]``
   ``pod``;``пол [pol]``
   ``radijator``;``радиатор [radiator]`` isto ``батарея [batarjeja]``
   ``terasa``;``балкон [balkon]``
   ``dečja soba``;``детская комната [djetskaja komnata]``
   ``kuhinja``;``кухня [kuhnja]``
   ``kupatilo``;``ванная [vannaja]``
   ``spavaća soba``;``спальня [spal'nja]``


