.. _samoglasnici:

=======================================
Samoglasnici / Гласные
=======================================

U ruskom jeziku ima 10 glasnih slova, ali glasova 6:

.. csv-table:: 
   :widths: 20, 20, 40
   :delim: ;
   :header: "Samoglasnik", "Glas", "Primer"

   ``а``; ``[a]``; ``aнанас [ananas] = ananas``
   ``у``; ``[u]``; ``уровень [urovjen'] = nivo``
   ``о``; ``[o]``; ``облако [oblako] = oblak``, ``облака [oblaka] = oblaci``
   ``и``; ``[i]``; ``игра [igra] = igra``
   ``э``; ``[e]``; ``эра [era] = era``
   ``ы``; ``[~i]``; ``тыл [t~il] = pozadina``
   ``я``; ``[ja]``; ``янтарь [jantar'] = ćilibar```
   ``ю``; ``[ju]``; ``люди [ljudi] = ljudi``
   ``е``; ``[je]``; ``печь [pjeć'] = peć``
   ``ё``; ``[jo]``; ``лён [ljon] = lan``


Glasni slova ``е``, ``ё``, ``ю``, ``я`` su kombinacija glasova.

