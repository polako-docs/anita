.. _alphabet:

============================================
Alphabet / Алфавит
============================================

Ruski alphabet ukupno ima 33 slova. Od kojih su 10 samoglasnici i 21 suglasnici. ``Ъ`` i ``Ь`` ne označavaju zvukove.

.. attention:: Princip «Piši kao što govoriš, a čitaj kao što je napisano» ne radi u ruskom jeziku. Isto slovo može imati različit glas.

.. csv-table:: 
   :widths: 20, 20, 40
   :delim: ;
   :header: "Slovo", "Izgovor", "Reč"

   ``А``; ``А``; Аптека [``aptjeka``] = Apoteka
   ``Б``; ``Be``; Банк [``bank``] = Banka
   ``В``; ``Ve``; Вино [``vino``] = Vino
   ``Г``; ``Ge``; Глаз [``glaz``] = Oko
   ``Д``; ``De``; Дом [``dom``] = Kuća
   ``E``; ``Je``; Ель [``jel'``] = Smreka
   ``Ё``; ``Jo``; Ёлка [``jolka``] = Jelka
   ``Ж``;``Že``; Жена [``žena``] = Supruga
   ``З``;``Ze``; Зонт [``zont``] = Kišobran
   ``И``;``I``; Игла [``igla``] = Igla
   ``Й``;``J`` (kratak i); Йод [``jod``] = Jod
   ``К``;``Ka``; Кофе [``kofje``] = Kafa
   ``Л``;``El``; Лампа [``lampa``] = Lampa
   ``М``;``Em``; Мост [``most``] = Most
   ``Н``;``En``; Нос [``nos``] = Nos
   ``О``;``O``; Овощи [``ovošći``] = Povrće
   ``П``;``Pe``; Парк [``park``] = Park
   ``Р``;``Re``; Рот [``rot``] = Usta
   ``С``;``Se``; Сад [``sad``] = Bašta
   ``Т``;``Te``; Торт [``tort``] = Torta
   ``У``;``U``; Урок [``urok``] = Čas
   ``Ф``;``Fe``; Фрукты [``frukt~i``] = Voće
   ``Х``;``Ha``; Хлеб [``hljeb``]= Hleb
   ``Ц``;``Ce``; Цветок [``cvjetok``] = Сvet
   ``Ч``;``Će``; Чай [``ćaj``] = Čaj
   ``Ш``;``Ša``; Шкаф [``škaf``] = Orman
   ``Щ``;``~Šća``; Щётка [``~šćjotka``] = Četka
   ``Ъ``; nema zvuka; --
   ``Ы``;``~I``; Сыр [``s~ir``] = Sir
   ``Ь``; nema zvuka; --
   ``Э``;``E``; Этаж [``etaž``] = Sprat
   ``Ю``;``Ju``; Юг [``jug``] = Jug
   ``Я``;``Ja``; Яблоко [``jabloko]`` = Jabuka





